#include "includes/server_manager.h"
#include "includes/server_actions.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Roee Seren");
MODULE_DESCRIPTION("A kernel socket server.");
MODULE_VERSION("0.1");  

// has to be global, so __exit function could terminate thread
struct task_struct * echo_server_thread;

int check_client_disconnected(struct task_struct ** client_threads) {
    int first_free_index = -1;
    bool should_update_index = false;
    int i;
    
    for (i = 0; i < MAX_CLIENTS; i++) {
        if (!client_threads[i]) {
            should_update_index = true;
        } else {
            if (client_threads[i]->exit_state !=  TASK_RUNNING) {
                should_update_index = true;
            }
        }

        // will happen only for first free index.
        if (should_update_index) {
            first_free_index = i;
            return first_free_index;
        }
    }
    return first_free_index;
}

int disconnect_all_clients(struct task_struct ** client_threads) {
    int i;
    int res = 0;
    for (i = 0; i < MAX_CLIENTS; i++) {
        // pick only running threads
        if (client_threads[i] && client_threads[i]->state == TASK_RUNNING) {
            res = kthread_stop(client_threads[i]);
            if (res < 0) {
                return res;
            }
        }
    }
    return res;
}

void configure_listen_socket(struct sockaddr_in * socket_address) {
    struct in_addr	ip_struct;

    ip_struct.s_addr = INADDR_ANY;
    socket_address->sin_family  = AF_INET;	                // Address family
    socket_address->sin_port    = htons(CONNECTION_PORT);	// Port number
    socket_address->sin_addr	= ip_struct;	            // Internet address
}

void connect_new_client(struct socket * listen_socket,
 struct task_struct ** client_threads) {
    int socket_ready = 0;
    int index;
    int res;

    struct socket * client_socket = NULL;
    while(1) {
        if (kthread_should_stop()) {
            if (client_socket) {
                kfree(client_socket);
            }
            break;
        }

        if (!socket_ready) {
            client_socket = kmalloc(1 * sizeof(struct socket), GFP_KERNEL);
            if (!client_socket) {
                printk(KERN_WARNING "Failed allocation memory for new user, exiting..\n");
                break;
            }
            socket_ready = 1;
        }
        
        res = kernel_accept(listen_socket, &client_socket, O_NONBLOCK);
        if (res < 0) {
            if (res == -EWOULDBLOCK || res == -EAGAIN) {
                continue;
            } else {
                printk(KERN_WARNING "Couldn't get new socket, stop running\n");
                break;
            }
        } else {
            /* new connection established, checking
               if new connection can be established and created new thread for client */
            index = check_client_disconnected(client_threads);
            if (index >= 0) {
                // register new thread to first free spot avilable in clients array
                client_threads[index] = kthread_run(client_echo_thread, client_socket, "client_thread");
                socket_ready = 0;
            } else {
                printk(KERN_WARNING "No clients available right now..\n");
            }
        }
    }
}

int server_thread_func(void* data) {
    struct task_struct * client_threads[MAX_CLIENTS] = {'\0'};
    struct socket * listen_socket = NULL;
    int listen_socket_fd;
    struct sockaddr_in socket_address;
    int res;

    // creates listen socket, configures it and binds it to correct configuration
    configure_listen_socket(&socket_address);
    
    // create new sokcet descriptor, sets to be based connection socket, ip family address
    listen_socket_fd = sock_create(AF_INET, SOCK_STREAM, IPPROTO_TCP ,&listen_socket);
    if (listen_socket_fd < 0) {
        printk(KERN_WARNING "Problem creating socket: %d\n", listen_socket_fd);
        return listen_socket_fd;
    }
    printk(KERN_INFO "Created socket successfully!\n");
    
    // configures socket to given port, adn ip address.
    res = kernel_bind(listen_socket, (struct sockaddr *)&socket_address, sizeof(socket_address));
    if (res < 0) {
        printk(KERN_INFO "Problem binding socket: %d\n", res);
        sock_release(listen_socket);
        return res;
    }
    printk(KERN_INFO "Bind socket successfully!\n");

    // make socket listen to new connections. 
    res = kernel_listen(listen_socket, MAX_CLIENTS);
    if (res < 0) {
        printk(KERN_WARNING "Problem listening socket %d\n", res);
        kernel_sock_shutdown(listen_socket, SHUT_RDWR);
        sock_release(listen_socket);    
        return res;
    }
    printk(KERN_INFO "Module started listening successfully!\n");

    // get new connections, and create new thread hadnling them
    connect_new_client(listen_socket, client_threads);
    
    // on thread exit, kills listen socket
    kernel_sock_shutdown(listen_socket, SHUT_RDWR);
    sock_release(listen_socket);    

    // on thread exit, kills client threads
    disconnect_all_clients(client_threads);

    return 0;
}
static int __init init_echoserver(void) {
    printk(KERN_INFO "Starting kernel echo server!\n");
    echo_server_thread = kthread_run(server_thread_func, NULL, "echo_server_thread");
    if (echo_server_thread < 0) {
        printk(KERN_WARNING "Failed creating echo server thread!\n");
        return -1;
    }
    return 0;
}

static void __exit exit_echoserver(void) {
    kthread_stop(echo_server_thread);
    printk(KERN_INFO "Goodbye!\n");
}

module_init(init_echoserver);
module_exit(exit_echoserver);
