#pragma once
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/net.h>
#include <linux/socket.h>
#include <linux/in.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include <linux/sched.h>

// recieve message statuses
#define STATUS_RECEIVED_SUCCESS 0
#define STATUS_RECEIVED_WAITING 1
#define STATUS_RECEIVED_ERROR -1
#define STATUS_USER_DISCONNECTED 2

// send messag status
#define STATUS_SEND_SUCCESS 0
#define STATUS_SEND_QUIT 1
#define STATUS_SEND_ERROR -1

#define MAX_MESSAGE_LENGTH 100

#define QUIT_MESSAGE "quit"

/* 
 * send back to user the message got from it (and saved in client_vector struct)
 * @client_socket: client socket to send message to
 * @client_vector: struct including message data
 * @client_message struct including message meta data necessary to send and receive
 * @return: 0 on message sent successfully
 *          1 on quit message
 *         -1 on error
 */
int send_message(struct socket * client_socket, struct kvec * client_vector,
 struct msghdr * client_message);

/* 
 * check if message is quit message signal from user, return true is so.
 * @message: message data
 */
bool should_quit(char * message);

/*
 * receive message from user (not blocking)
 * @client_socket: client socket to send message to
 * @client_vector: struct including message data
 * @client_message struct including message meta data necessary to send and receive
 * @message_length: maximum message length to be receive by user
 * @recvmsg_flags: flags for kernel_recvmsg function (for example, to make it a non-blocking function)
 * return: 0 on success recieveing message
 *         1 on waiting for message
 *        -1 on error
 */
int recieve_message(struct socket * client_socket, struct msghdr * client_message,
    struct kvec * client_vector, int message_length, int recvmsg_flags);