#pragma once
// #include <linux/kernel.h>
#include <linux/module.h>
#pragma once
#include <linux/net.h>
#include <linux/socket.h>
#include <linux/in.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include "includes/client_manager.h"


#define MAX_CLIENTS 10
#define CONNECTION_PORT 16633

/*  
 *  cleans available threads for new clients,
 *  and returns first index of free thread.
 *  if no threads are available, returns -1.
 *  @client_thread: thread pointer array stating all active threads running. 
 */
int check_client_disconnected(struct task_struct ** client_threads);

/* 
 * iterates client array and close open threads
 * @client_threads: array of client threads 
 * @return: 0 on success, <0 on error
 */
int disconnect_all_clients(struct task_struct ** client_threads);

/*
 * configure listen socket to default tcp socket configuration
 * on pre defined port.
 * @socket_address: pointer to sockaddr_in struct that will be configured
 */
void configure_listen_socket(struct sockaddr_in * socket_address);

/*
 * waits for new client to connect (not blocking), create seperate thread for it
 * and appends new thread to client_threads array.
 * @listen_socket: main servr socket that listens to new clients connecting.
 * @client_threads: array of all current connected clients (thread for each client)
 */
void connect_new_client(struct socket * listen_socket,
 struct task_struct ** client_threads);

/* 
 * main server function. creates main listening socket,
 * waits for new connection, and on stop free main listening socket
 */
 int server_thread_func(void* data);

/*
 * load module function. creates new thread,
 * the server thread.
 */ 
 static int __init init_echoserver(void);

/*
 * unload module function. close all clients threads,
 * and closes server thread.
 */
 static void __exit exit_echoserver(void);