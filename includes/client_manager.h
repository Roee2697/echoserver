#pragma once
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/net.h>
#include <linux/socket.h>
#include <linux/in.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include <linux/sched.h>

// recieve message statuses
#define STATUS_RECEIVED_SUCCESS 0
#define STATUS_RECEIVED_WAITING 1
#define STATUS_RECEIVED_ERROR -1
#define STATUS_USER_DISCONNECTED 2

// send messag status
#define STATUS_SEND_SUCCESS 0
#define STATUS_SEND_QUIT 1
#define STATUS_SEND_ERROR -1

#define MAX_MESSAGE_LENGTH 100
#define MAX_CLIENTS 10

/*
 * confiugres msghr struct including meta data to send and receive messages
 * @client_message: pointer to msghdr struct to configure
 */
void configure_client_message(struct msghdr * client_message);

/*
 * confiugres kvec struct - container for message data from client
 * @client_message: pointer to kvec struct to configure
 */
void configure_client_vector(struct kvec * client_vector, char * message, int max_message_length);

/*
 * main function for each client.
 * waiting for new message received from user, and send it back.
 * on close, free client socket and close thread
 * @client_socket: socket for the client to receive and send messages
 * @return: 0 on success, <0 err value otherwise
 */
int client_echo_thread(void * client_socket);