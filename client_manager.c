#include "includes/client_manager.h"
#include "includes/server_actions.h"

void configure_client_message(struct msghdr * client_message) {
    client_message->msg_name = 0;
    client_message->msg_namelen = 0;
    client_message->msg_control = NULL;
    client_message->msg_controllen = 0;
    client_message->msg_flags = 0;
}

void configure_client_vector(
    struct kvec * client_vector, 
    char * message, 
    int max_message_length
    ) {
    // clear message data memory
    memset(message, 0, MAX_MESSAGE_LENGTH);

    // configure client vector by given args
    client_vector->iov_len = MAX_MESSAGE_LENGTH;
    client_vector->iov_base = message;
}

int client_echo_thread(void * client_socket) {
    int res;
    int receive_message; 
    unsigned char message_data[MAX_MESSAGE_LENGTH];
    struct msghdr client_message;
    struct kvec client_vector;
    bool should_continue = true;

    configure_client_vector(&client_vector, message_data, MAX_MESSAGE_LENGTH);
    configure_client_message(&client_message);

    while(should_continue) {
        if (kthread_should_stop()) {
            set_current_state(TASK_INTERRUPTIBLE);
            break;
        }

        // get message from user (non blocking)
        receive_message = recieve_message(client_socket, &client_message, 
            &client_vector, MAX_MESSAGE_LENGTH, MSG_DONTWAIT);
        switch (receive_message)
        {
            case STATUS_RECEIVED_SUCCESS:
                res = send_message(client_socket, &client_vector, &client_message);
                should_continue = res == STATUS_SEND_QUIT;
                break;
            case STATUS_RECEIVED_WAITING:
                // in case no action was done, and server keeps waiting for client message
                continue;
            default:
                should_continue = false;
                break;
        }
    }

    if (client_socket) {
        kernel_sock_shutdown(client_socket, SHUT_RDWR);
        sock_release(client_socket);
    }
    printk(KERN_INFO "User done sending messages!\n");
    return 0;
}