#include "includes/server_actions.h"

bool should_quit(char * message) {
    return (memcmp(message, QUIT_MESSAGE, strlen(QUIT_MESSAGE)) != 0);
}

/* 
    Get message from user, and return status
    0  - success recieveing message
    1  - waiting for message to come
    -1 - error

*/
int recieve_message(struct socket * client_socket, struct msghdr * client_message,
    struct kvec * client_vector, int message_length,int recvmsg_flags) {
    
    int res;
    res = kernel_recvmsg(client_socket, client_message, client_vector,
     message_length, message_length, MSG_DONTWAIT);
    if (res < 0) {
        if (res == -EAGAIN || res == -EWOULDBLOCK) {
            return STATUS_RECEIVED_WAITING;
        } else {
            printk(KERN_WARNING "Problem getting message from user: %d\n", res);
            if (client_socket) {
                kernel_sock_shutdown(client_socket, SHUT_RDWR);
                sock_release(client_socket);
            }
            return STATUS_RECEIVED_ERROR;
        }
    } else if (res == 0) {
        printk(KERN_INFO "Connection disconnected\n");
        return STATUS_USER_DISCONNECTED;
    } else {
        return  STATUS_RECEIVED_SUCCESS;
    }
}

/*
    return 0 on success (normal message)
    return 1 on sucees (quit message)
    -1 on error
*/
int send_message(struct socket * client_socket, struct kvec * client_vector,
 struct msghdr * client_message) {
    int res;
    char * message; 

    message = client_vector->iov_base;
    printk(KERN_INFO "Got message from user succcessfully! %s\n", message);

    // send message back to user
    res = kernel_sendmsg(client_socket, client_message, client_vector, strlen(message), strlen(message));
    if (res < 0) {
        printk(KERN_WARNING "Error sending message to user %d", res);
        memset(message, 0, MAX_MESSAGE_LENGTH);
        return STATUS_SEND_ERROR;
    }
    // if message is 'quit', signals thread to stop.
    if (should_quit(message)) {
        memset(message, 0, MAX_MESSAGE_LENGTH);
        return STATUS_SEND_QUIT;
    }

    memset(message, 0, MAX_MESSAGE_LENGTH);
    return STATUS_SEND_SUCCESS;
}